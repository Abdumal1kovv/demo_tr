from django.urls import path
from .views import UserList, UserDetail, UserUpdate, UserDelete, UserSearch, Search
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('users-all-or-create/', UserList.as_view(), name='user-list-or-create'),
    path('user/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('user/<int:pk>/update/', UserUpdate.as_view(), name='user-update'),
    path('user/<int:pk>/delete/', UserDelete.as_view(), name='user-delete'),
    path('user/search/username/', UserSearch.as_view(), name='user-search-username'),
    path('user/search/', Search.as_view(), name='user-search'),

    path('api/token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
]
